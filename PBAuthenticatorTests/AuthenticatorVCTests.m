//  AuthenticatorVCTests.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "AuthenticatorVC.h"
#import "AddTokenVC.h"
#import "PBAuthenticatorTestCase.h"
#import "HOTP.h"
#import "TOTP.h"
#import "HOTPTableViewCell.h"
#import "TOTPTableViewCell.h"
#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>

#define STORYBOARD_ID @"AuthenticatorVC"
#define STORYBOARD_PHONE @"MainStoryboard_iPhone"

#define AUTENTICATORVC_TITLE @"PBAuthenticator"

#define CELL_IDENT_TOTP @"cell_totp"
#define CELL_IDENT_HOTP @"cell_hotp"

@interface AuthenticatorVC (TESTING)

@property (nonatomic, strong, readwrite) NSDate *currentDate;

@end

@implementation AuthenticatorVC (TESTING)

@dynamic currentDate;

@end

@interface AuthenticatorVCTests : PBAuthenticatorTestCase
@end

@implementation AuthenticatorVCTests {
    UIStoryboard *sbPhone;
    AuthenticatorVC *sutPhone; // System Under Test from Phone StoryBoard
    AuthenticatorVC *sut;
    UITableView *mockTableView;
    HOTPTableViewCell *mockCellHOTP;
    TOTPTableViewCell *mockCellTOTP;
    UILabel *mockCellHOTPNameLabel;
    UILabel *mockCellHOTPPasscodeLabel;
    UIButton *mockCellHOTPNextPasscodeBtn;
    id <HOTPTableViewCellDelegate> mockCellHOTPDelegate;
    UILabel *mockCellTOTPNameLabel;
    UILabel *mockCellTOTPPasscodeLabel;
    HOTP *mockHOTP;
    TOTP *mockTOTP;
    NSArray *testOTPRecords;
}

- (void)setUp
{
    [super setUp];

    sbPhone = [UIStoryboard storyboardWithName:STORYBOARD_PHONE bundle:nil];
    sutPhone = [sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID];
    [sutPhone view];
    sut = [[AuthenticatorVC alloc] init];
    [sut view];
}

- (void)tearDown
{
    sut = nil;
    [super tearDown];
}

- (void)setupSUTCurrentDateForTimeIntervalSince1970:(NSTimeInterval)timeInterval
{
    [sutPhone setCurrentDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
    [sut setCurrentDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
}

- (void)testSBPhoneIdentifierSet
{
    XCTAssertNoThrow([sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID]);
}

- (void)testSBPhoneSceneHasClassSet
{
    sutPhone = [sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID];
    assertThat([sutPhone class], is(equalTo([AuthenticatorVC class])));
}

- (void)testSBPhoneSceneTitleSet
{
    assertThat([sutPhone title], is(equalTo(AUTENTICATORVC_TITLE)));
}

- (void)testNavigationBarTitleIsSetWithVCTitle
{
    assertThat([[sutPhone navigationItem] title], is(equalTo(AUTENTICATORVC_TITLE)));
}

- (void)testSBPhonePlusButtonConnected
{
     assertThat([sutPhone btn_addToken], is(notNilValue()));
}

- (void)testSBPhonePlusButtonSegue
{
    [self doTestBarButtonItem:[sutPhone btn_addToken] performsSegueWithIdentifier:@"AuthenticatorVC_btn_to_AddTokenVC"];
}

- (void)testSBPhoneCountdownButtonConnected
{
    assertThat([sutPhone btn_countdown], is(notNilValue()));
}

- (void)testCountdownSecondsRemainingInFirstHalfOfMinute
{
    // 01/08/2013 11:46:12 (GMT) = 1375357572
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357572];
    assertThatInt([sut countdownSecondsRemaining], is(equalToInt(18)));
}

- (void)testCountdownSecondsRemainingInSecondHalfOfMinute
{
    // 01/08/2013 11:46:52 (GMT) = 1375357612
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357612];
    assertThatInt([sut countdownSecondsRemaining], is(equalToInt(8)));
}

- (void)testTableReloadedWhenCountdownSecondsRemainingIs30
{
    // 01/08/2013 11:47:00 (GMT) = 1375357620
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357620];
    
    mockTableView = mock([UITableView class]);

    [sutPhone setTableView:mockTableView];
    
    [sutPhone startTOTPUpdates];
    
    [verify(mockTableView) reloadData];
}

- (void)testTableNotReloadedWhenCountdownSecondsRemainingIsNot30
{
    // 01/08/2013 11:46:59 (GMT) = 1375357619
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357619];
    
    mockTableView = mock([UITableView class]);
    
    [sutPhone setTableView:mockTableView];
    
    [sutPhone startTOTPUpdates];
    
    [verifyCount(mockTableView, never()) reloadData];
}


- (void)testSBPhoneButtonTextIsSetToCountdownSecondsRemaining
{
    // 01/08/2013 11:46:53 (GMT) = 1375357613
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357613];
    
    [sutPhone startTOTPUpdates];

    assertThat([[sutPhone btn_countdown] title], is(equalTo(@"7")));
}

- (void)testTableReloadedWhenCountdownSecondsRemainingIs5
{
    // 01/08/2013 11:46:55 (GMT) = 1375357615
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357615];
    
    mockTableView = mock([UITableView class]);
    
    [sutPhone setTableView:mockTableView];
    
    [sutPhone startTOTPUpdates];
    
    [verify(mockTableView) reloadData];
}

- (void)testTableViewCellPasscodeLabelTextColorIsSetRedForTOTPWhen5SecondsRemaining
{
    // 01/08/2013 11:46:55 (GMT) = 1375357615
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357615];
    
    [self setTestData];
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];

    [verify(mockCellTOTPPasscodeLabel) setTextColor:[UIColor redColor]];
}

- (void)testTableViewCellPasscodeLabelTextColorIsSetBlackForTOTPWhen30SecondsRemaining
{
    // 01/08/2013 11:47:00 (GMT) = 1375357620
    [self setupSUTCurrentDateForTimeIntervalSince1970:1375357620];
    
    [self setTestData];
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];
    
    [verify(mockCellTOTPPasscodeLabel) setTextColor:[UIColor blackColor]];
}

- (void)testSBPhoneUnwindSegueForAddTokenCancelled
{
    assertThatBool([[[AuthenticatorVC alloc] init] canPerformUnwindSegueAction:@selector(addTokenCancelled:) fromViewController:nil withSender:nil], is(equalToBool(YES)));
}

- (void)testSBPhoneUnwindSegueForAddTokenDone
{
    assertThatBool([[[AuthenticatorVC alloc] init] canPerformUnwindSegueAction:@selector(addTokenDone:) fromViewController:nil withSender:nil], is(equalToBool(YES)));
}

- (void)testAuthenticatorConformsToTableViewDatasourceProtocol
{
    assertThatBool([sutPhone conformsToProtocol:@protocol(UITableViewDataSource)], is(equalToBool(YES)));
}

- (void)testAuthenticatorConformsToTableViewDelegateProtocol
{
    assertThatBool([sutPhone conformsToProtocol:@protocol(UITableViewDelegate)], is(equalToBool(YES)));
}

- (void)testAuthenticatorTableViewConnected
{
    assertThat([sutPhone tableView], is(notNilValue()));
}

- (void)testAuthenticatorTableViewDataSourceConnected
{
    assertThat(sutPhone.tableView.dataSource, is(equalTo(sutPhone)));
}

- (void)testAuthenticatorTableViewDelegateConnected
{
    assertThat(sutPhone.tableView.delegate, is(equalTo(sutPhone)));
}

- (void)test2RowsOfOTPData
{
    [self setTestData];

    [sutPhone setOtpRecords:testOTPRecords];
     
    assertThatInteger([sutPhone tableView:mockTableView numberOfRowsInSection:0], is(equalToInteger([testOTPRecords count])));
}

- (void)testTableViewCellNameLabelConnectedForHOTP
{
    OTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_HOTP];
    assertThat([cell lbl_name], is(notNilValue()));
}

- (void)testTableViewCellPasscodeLabelConnectedForHOTP
{
    OTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_HOTP];
    assertThat([cell lbl_passcode], is(notNilValue()));
}

- (void)testTableViewCellNameLabelConnectedForTOTP
{
    OTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_TOTP];
    assertThat([cell lbl_name], is(notNilValue()));
}

- (void)testTableViewCellPasscodeLabelConnectedForTOTP
{
    OTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_TOTP];
    assertThat([cell lbl_passcode], is(notNilValue()));
}

- (void)testTableViewCellNextPasscodeBtnConnected
{
    HOTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_HOTP];
    assertThat([cell btn_nextPasscode], is(notNilValue()));
}

- (void)testConformsToHOTPCellDelegate
{
    assertThatBool([sutPhone conformsToProtocol:@protocol(HOTPTableViewCellDelegate)], is(equalToBool(YES)));
}
- (void)testTableViewHOTPCellDelegateIsConnected
{
    HOTPTableViewCell *cell = [sutPhone.tableView dequeueReusableCellWithIdentifier:CELL_IDENT_HOTP];
    assertThat([cell delegate], is(equalTo(sutPhone)));
}


- (void)testTableViewCellCreatedForHOTP
{
    [self setTestData];
    
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
        
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    assertThat([sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath], is(equalTo(mockCellHOTP)));
}

- (void)testTableViewCellCreatedForTOTP
{
    [self setTestData];
    
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    assertThat([sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath], is(equalTo(mockCellTOTP)));
}

- (void)testTableViewCellNameLabelTextIsSetForHOTP
{
    [self setTestData];
    
    [given([mockHOTP name]) willReturn:@"testHOTPName"];
    
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];
    [verify(mockCellHOTPNameLabel) setText:@"testHOTPName"];
}

- (void)testTableViewCellPasscodeLabelTextIsSetForHOTP
{
    [self setTestData];
    
    [given([mockHOTP passcode]) willReturn:@"testHOTPPasscode"];
    
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];
    [verify(mockCellHOTPPasscodeLabel) setText:@"testHOTPPasscode"];
}

- (void)testTableViewCellNameLabelTextIsSetForTOTP
{
    [self setTestData];

    [given([mockTOTP name]) willReturn:@"testTOTPName"];

    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
        
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];
    [verify(mockCellTOTPNameLabel) setText:@"testTOTPName"];
}

- (void)testTableViewCellPasscodeLabelTextIsSetForTOTP
{
    [self setTestData];
    
    [given([mockTOTP passcode]) willReturn:@"testTOTPPasscode"];
    
    [sutPhone setOtpRecords:testOTPRecords];
    
    [self setMocksForTableTests];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    [sutPhone tableView:mockTableView cellForRowAtIndexPath:indexPath];
    [verify(mockCellTOTPPasscodeLabel) setText:@"testTOTPPasscode"];
}

- (void)testTableViewDataSourceIsConnected
{
    assertThat([[sutPhone tableView] dataSource], is(equalTo(sutPhone)));
}

- (void)testConformsToProtocol
{
    assertThatBool([sut conformsToProtocol:@protocol(AddTokenDelegate)], is(equalToBool(YES)));
}

- (void)testDelegateIsSetBeforeSegueToAddTokenVC
{
    AddTokenVC *mockDestination = mock([AddTokenVC class]);
    
    UIStoryboardSegue *segue = [UIStoryboardSegue segueWithIdentifier:@"AuthenticatorVC_btn_to_AddTokenVC" source:sutPhone destination:mockDestination performHandler:^{
        // no action required
    }];
    [sutPhone prepareForSegue:segue sender:nil];
    [(AddTokenVC *)verify(mockDestination) setDelegate:sutPhone];
}

- (void)testDidCreateNewOTPAddsHOTPRecordToOTPRecords
{
    NSUInteger beforeCount = [sut.otpRecords count];
    
    [sut didCreateNewOTP:[[HOTP alloc] init]];

    assertThatInteger([sut.otpRecords count], is(equalToInteger(beforeCount + 1)));
}

- (void)testDidCreateNewOTPAddsTOTPRecordToOTPRecords
{
    NSUInteger beforeCount = [sut.otpRecords count];
    
    [sut didCreateNewOTP:[[TOTP alloc] init]];
    
    assertThatInteger([sut.otpRecords count], is(equalToInteger(beforeCount + 1)));
}

- (void)testDidCreateNewOTPReloadsTableView
{
    mockTableView = mock([UITableView class]);

    sut.tableView = mockTableView;
    
    [sut didCreateNewOTP:[[HOTP alloc] init]];
    
    [verify(mockTableView) reloadData];
}

- (void)testRowHeightIsSet
{
    assertThatInteger([sut tableView:(id)anything() heightForRowAtIndexPath:(id)anything()], is(equalToInteger(110)));
}

- (void)testSBPhoneEditButtonConnected
{
    assertThat([sutPhone btn_edit], is(notNilValue()));
}

- (void)testSBPhoneEditButtonHookedUpToEditMode
{
    UIBarButtonItem *button = sutPhone.btn_edit;

    assertThat([button target], is(equalTo(sutPhone)));
    assertThat(NSStringFromSelector([button action]), is(equalTo(@"editMode:")));
}

- (void)testEditModeActionSetsTableToEditMode
{
    mockTableView = mock([UITableView class]);
    [sut setTableView:mockTableView];
    
    [sut editMode:nil];

    [verify(mockTableView) setEditing:YES animated:YES];
}

- (void)testEditModeActionReturnsTableFromEditMode
{
    mockTableView = mock([UITableView class]);
    [sut setTableView:mockTableView];

    [given([mockTableView isEditing]) willReturnBool:YES];
    
    [sut editMode:nil];
    
    [verify(mockTableView) setEditing:NO animated:YES];
}

- (void)testEditModeSetsButtonTextToEdit
{
    mockTableView = mock([UITableView class]);
    UIBarButtonItem *mockBtn = mock([UIBarButtonItem class]);
    [sut setTableView:mockTableView];
    [sut setBtn_edit:mockBtn];
    
    [given([mockTableView isEditing]) willReturnBool:NO];

    [sut editMode:mockBtn];
    
    [verify(mockBtn) setTitle:@"Done"];
    [(UIBarButtonItem *)verify(mockBtn) setStyle:UIBarButtonItemStyleDone];
}

- (void)testEditModeSetsButtonTextToDone
{
    mockTableView = mock([UITableView class]);
    UIBarButtonItem *mockBtn = mock([UIBarButtonItem class]);
    [sut setTableView:mockTableView];
    [sut setBtn_edit:mockBtn];
    
    [given([mockTableView isEditing]) willReturnBool:YES];
    
    [sut editMode:nil];
    
    [verify(mockBtn) setTitle:@"Edit"];
    [(UIBarButtonItem *)verify(mockBtn) setStyle:UIBarButtonItemStyleBordered];
}

- (void)testOTPDeletedAndTableReloads
{
    mockTableView = mock([UITableView class]);
    [sutPhone setTableView:mockTableView];
    
    [self setTestData];
    [sutPhone setOtpRecords:testOTPRecords];
    
    [sutPhone tableView:mockTableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    
    assertThatInteger([sutPhone.otpRecords count], is(equalToInteger(1)));
    [verify(mockTableView) reloadData];
}

#pragma mark - helper methods

- (void)setMocksForTableTests
{
    mockTableView = mock([UITableView class]);
    mockCellHOTP = mock([HOTPTableViewCell class]);
    mockCellTOTP = mock([TOTPTableViewCell class]);
    mockCellHOTPNameLabel = mock([UILabel class]);
    mockCellHOTPPasscodeLabel = mock([UILabel class]);
    mockCellHOTPNextPasscodeBtn = mock([UIButton class]);
    mockCellHOTPDelegate = mockObjectAndProtocol([NSObject class], @protocol(HOTPTableViewCellDelegate));
    mockCellTOTPNameLabel = mock([UILabel class]);
    mockCellTOTPPasscodeLabel = mock([UILabel class]);
    
    [given([mockCellHOTP lbl_name]) willReturn:mockCellHOTPNameLabel];
    [given([mockCellHOTP lbl_passcode]) willReturn:mockCellHOTPPasscodeLabel];
    [given([mockCellHOTP btn_nextPasscode]) willReturn:mockCellHOTPNextPasscodeBtn];
    [given([mockCellHOTP delegate]) willReturn:mockCellHOTPDelegate];
    [given([mockCellTOTP lbl_name]) willReturn:mockCellTOTPNameLabel];
    [given([mockCellTOTP lbl_passcode]) willReturn:mockCellTOTPPasscodeLabel];
    [given([mockTableView dequeueReusableCellWithIdentifier:CELL_IDENT_HOTP]) willReturn:mockCellHOTP];
    [given([mockTableView dequeueReusableCellWithIdentifier:CELL_IDENT_TOTP]) willReturn:mockCellTOTP];
}

- (void)setTestData
{
    mockHOTP = mock([HOTP class]);
    mockTOTP = mock([TOTP class]);
        
    testOTPRecords = @[mockHOTP, mockTOTP];
}

@end
