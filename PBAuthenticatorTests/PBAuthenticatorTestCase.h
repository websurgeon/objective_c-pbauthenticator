//  PBAuthenticatorTestCase.h
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <XCTest/XCTest.h>

@class UIBarButtonItem, NSString;

@interface PBAuthenticatorTestCase : XCTestCase

- (void)doTestBarButtonItem:(UIBarButtonItem *)button performsSegueWithIdentifier:(NSString *)segueIdentifier;

@end