//  AddTokenVCTests.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "AddTokenVC.h"
#import "AuthenticatorVC.h"
#import "PBAuthenticatorTestCase.h"
#import "PBMockAlertView.h"
#import "PBMockAlertViewVerifier.h"
#import "OTP.h"
#import "TOTP.h"
#import "HOTP.h"
#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>

#define STORYBOARD_ID @"AddTokenVC"
#define STORYBOARD_PHONE @"MainStoryboard_iPhone"
#define SUT_TITLE @"Add Token"

@interface AddTokenVCTests : PBAuthenticatorTestCase
@end

@implementation AddTokenVCTests {
    UIStoryboard *sbPhone;
    AddTokenVC *sutPhone; // System Under Test from Phone StoryBoard
    AddTokenVC *sut;
}

- (void)setUp
{
    [super setUp];
    
    sbPhone = [UIStoryboard storyboardWithName:STORYBOARD_PHONE bundle:nil];
    sutPhone = [sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID];
    [sutPhone view];
    sut = [[AddTokenVC alloc] init];
    [sut view];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testSBPhoneIdentifierSet
{
    XCTAssertNoThrow([sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID]);
}

- (void)testSBPhoneSceneHasClassSet
{
    sutPhone = [sbPhone instantiateViewControllerWithIdentifier:STORYBOARD_ID];
    assertThat([sutPhone class], is(equalTo([AddTokenVC class])));
}

- (void)testSBPhoneSceneTitleSet
{
    assertThat([sutPhone title], is(equalTo(SUT_TITLE)));
}

- (void)testNavigationBarTitleIsSetWithVCTitle
{
    assertThat([[sutPhone navigationItem] title], is(equalTo(SUT_TITLE)));
}

- (void)testSBPhoneCancelButtonConnected
{
    assertThat([sutPhone btn_cancel], is(notNilValue()));
}

- (void)testSBPhoneOTPChoiceSegmentedControlConnected
{
    assertThat([sutPhone seg_otpChoice], is(notNilValue()));
}

- (void)testSBPhoneCancelButtonPerformsUnwindSegue
{
    [self doTestBarButtonItem:[sutPhone btn_cancel] performsSegueWithIdentifier:@"Unwind_addTokenCancelled"];
}

- (void)testSBPhoneDoneButtonConnected
{
    assertThat([sutPhone btn_done], is(notNilValue()));
}

- (void)testSBPhoneTextFieldForKeyIsConnected
{
    assertThat([sutPhone input_key], is(notNilValue()));
}

- (void)testSBPhoneTextFieldForNameIsConnected
{
    assertThat([sutPhone input_name], is(notNilValue()));
}

- (void)testSBPhoneDoneButtonPerformsUnwindSegue
{
    [self doTestBarButtonItem:[sutPhone btn_done] performsSegueWithIdentifier:@"Unwind_addTokenDone"];
}

- (void)testSegueInvokesDidCreateNewOTPDelegateMethodWithTOTPChoiceSelected
{
    UISegmentedControl *mockOTPChoice = mock([UISegmentedControl class]);
    id <AddTokenDelegate> mockDelegate = mockObjectAndProtocol([NSObject class], @protocol(AddTokenDelegate));
    sutPhone.delegate = mockDelegate;
    sutPhone.seg_otpChoice = mockOTPChoice;
    
    [given([mockOTPChoice selectedSegmentIndex]) willReturnInt:0];
    
    AuthenticatorVC *mockDestination = mock([AuthenticatorVC class]);
    UIStoryboardSegue *segue = [UIStoryboardSegue segueWithIdentifier:@"Unwind_addTokenDone" source:sutPhone destination:mockDestination performHandler:^{
        // no aciton required
    }];
    [sutPhone prepareForSegue:segue sender:nil];
    
    [verify(mockDelegate) didCreateNewOTP:(id)anything()];
}

- (void)testSegueInvokesDidCreateNewOTPDelegateMethodWithHOTPChoiceSelected
{
    UISegmentedControl *mockOTPChoice = mock([UISegmentedControl class]);
    id <AddTokenDelegate> mockDelegate = mockObjectAndProtocol([NSObject class], @protocol(AddTokenDelegate));
    sutPhone.delegate = mockDelegate;
    sutPhone.seg_otpChoice = mockOTPChoice;
    
    [given([mockOTPChoice selectedSegmentIndex]) willReturnInt:1];
    
    AuthenticatorVC *mockDestination = mock([AuthenticatorVC class]);
    UIStoryboardSegue *segue = [UIStoryboardSegue segueWithIdentifier:@"Unwind_addTokenDone" source:sutPhone destination:mockDestination performHandler:^{
        // no aciton required
    }];
    [sutPhone prepareForSegue:segue sender:nil];
    
    [verify(mockDelegate) didCreateNewOTP:(id)anything()];
}

- (void)testUnwindSegueWhenDoneTappedDoesNotHappenWhenNoKeySet
{
    assertThatBool([sutPhone shouldPerformSegueWithIdentifier:@"Unwind_addTokenDone" sender:nil], is(equalToBool(NO)));
}

- (void)testAlertIsShownWhenUnwindSegueIsPreventDueToInvalidInputs
{
    PBMockAlertViewVerifier *alertViewVerifier = [[PBMockAlertViewVerifier alloc] init];
    UITextField *mockInputKey = mock([UITextField class]);
    [sutPhone setAlertViewClass:[PBMockAlertView class]];
    sutPhone.input_key = mockInputKey;
    
    [given([mockInputKey text]) willReturn:@""];

    [sutPhone shouldPerformSegueWithIdentifier:@"Unwind_addTokenDone" sender:nil];

    assertThatInteger([alertViewVerifier showCount], is(equalToInteger(1)));
}

- (void)testAlertIsNotShownWhenOTPDoneSegueHappens
{
    PBMockAlertViewVerifier *alertViewVerifier = [[PBMockAlertViewVerifier alloc] init];
    UITextField *mockInputKey = mock([UITextField class]);
    [sutPhone setAlertViewClass:[PBMockAlertView class]];
    sutPhone.input_key = mockInputKey;
    
    [given([mockInputKey text]) willReturn:@"validkey"];
    
    [sutPhone shouldPerformSegueWithIdentifier:@"Unwind_addTokenDone" sender:nil];
    
    assertThatInteger([alertViewVerifier showCount], is(equalToInteger(0)));
}

- (void)testNewOTPNameIsSetFromAccountText
{
    UISegmentedControl *mockOTPChoice = mock([UISegmentedControl class]);
    UITextField *mockInputField = mock([UITextField class]);
    sutPhone.seg_otpChoice = mockOTPChoice;
    sutPhone.input_name = mockInputField;

    [given([mockInputField text]) willReturn:@"newOTP"];
    [given([mockOTPChoice selectedSegmentIndex]) willReturnInt:1];
    OTP *newOTP = [sutPhone createNewOTPFromInputs];

    assertThat([newOTP name], is(equalTo(@"newOTP")));
}

@end