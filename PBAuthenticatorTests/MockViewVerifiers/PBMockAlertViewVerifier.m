//
//  PVMockAlertViewVerifier.m
//  PBAuthenticator
//
//  Copied / Adapted from Jonathan M. Reid's JMRMockAlertViewVerifier
//  https://github.com/jonreid/iOSAlertViewActionSheetUnitTesting
//
//  Created by Peter on 05/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "PBMockAlertViewVerifier.h"
#import "PBMockAlertView.h"

@implementation PBMockAlertViewVerifier

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(alertShown:)
                                                     name:PBMockAlertViewShowNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)alertShown:(NSNotification *)notification
{
    PBMockAlertView *alert = [notification object];
	++_showCount;
	[self setTitle:[alert title]];
	[self setMessage:[alert message]];
	[self setDelegate:[alert delegate]];
	[self setCancelButtonTitle:[alert cancelButtonTitle]];
	[self setOtherButtonTitles:[alert otherButtonTitles]];
}

@end
