//
//  PBMockAlertViewVerifier.h
//  PBAuthenticator
//
//  Copied / Adapted from Jonathan M. Reid's JMRMockAlertViewVerifier
//  https://github.com/jonreid/iOSAlertViewActionSheetUnitTesting
//
//  Created by Peter on 05/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <Foundation/Foundation.h>

@interface PBMockAlertViewVerifier : NSObject

@property (nonatomic, assign) NSUInteger showCount;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, strong) id delegate;
@property (nonatomic, copy) NSString *cancelButtonTitle;
@property (nonatomic, copy) NSArray *otherButtonTitles;

- (id)init;

@end
