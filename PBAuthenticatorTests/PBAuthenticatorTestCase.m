//  PBAuthenticatorTestCase.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "PBAuthenticatorTestCase.h"
#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define UIStoryboardSegueTemplate @"UIStoryboardSegueTemplate"

@import UIKit;

@implementation PBAuthenticatorTestCase

- (void)doTestBarButtonItem:(UIBarButtonItem *)button performsSegueWithIdentifier:(NSString *)segueIdentifier
{

    SEL selector = [button action];
    assertThat(NSStringFromSelector(selector), is(equalTo(@"perform:")));
    
    id target = [button target];
    assertThat(NSStringFromClass([[target class] superclass]), is(equalTo(UIStoryboardSegueTemplate
                                                                  )));
    
    if ([NSStringFromClass([[target class] superclass]) isEqualToString:UIStoryboardSegueTemplate]) {
        assertThat([target identifier], is(equalTo(segueIdentifier)));
    }
}

@end
