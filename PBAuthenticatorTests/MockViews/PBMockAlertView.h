//
//  PBMockAlertView.h
//  PBAuthenticator
//
//  Copied / Adapted from Jonathan M. Reid's JMRMockAlertView
//  https://github.com/jonreid/iOSAlertViewActionSheetUnitTesting
//
//  Created by Peter on 05/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <UIKit/UIKit.h>

extern NSString * const PBMockAlertViewShowNotification;

@interface PBMockAlertView : UIView


@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) id delegate;
@property (strong, nonatomic) NSString *cancelButtonTitle;
@property (strong, nonatomic) NSMutableArray *otherButtonTitles;

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

- (void)show;

@end
