//
//  PBMockAlertView.m
//  PBAuthenticator
//
//  Copied / Adapted from Jonathan M. Reid's JMRMockAlertView
//  https://github.com/jonreid/iOSAlertViewActionSheetUnitTesting
//
//  Created by Peter on 05/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "PBMockAlertView.h"

@implementation PBMockAlertView

NSString * const PBMockAlertViewShowNotification = @"PBMockAlertViewShowNotification";

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...
{
    self = [super init];
    if (self) {
        _title = [title copy];
        _message = [message copy];
        _delegate = delegate;
        _cancelButtonTitle = [cancelButtonTitle copy];
        _otherButtonTitles = [[NSMutableArray alloc] init];
        va_list args;
        va_start(args, otherButtonTitles);
        for (NSString *title = otherButtonTitles; title != nil; title = va_arg(args, NSString *)) {
            [_otherButtonTitles addObject:title];
        }
        va_end(args);
    }
    return self;
}

- (void)show
{
    [[NSNotificationCenter defaultCenter] postNotificationName:PBMockAlertViewShowNotification object:self userInfo:nil];
}

@end
