//  OTPTest.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "OTP.h"

#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define TEST1_KEY  @"GAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQ" // @"00000000000000000000"
#define TEST1_EXPECTED @[@"730449",@"379922",@"946318",@"130560",@"220896",@"832455",@"182563"]
#define TEST1_STARTTIME 1375357560 // {2013,8,1,12,46,0}
#define TEST1_DIGITS 6

//    secret = GAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQ
//    01/08/2013 11:46:00 (GMT) = 1375357560 => 730449
//    01/08/2013 11:46:30 (GMT) = 1375357590 => 379922
//    01/08/2013 11:47:00 (GMT) = 1375357620 => 946318
//    01/08/2013 11:47:30 (GMT) = 1375357650 => 130560
//    01/08/2013 11:48:00 (GMT) = 1375357680 => 220896
//    01/08/2013 11:48:30 (GMT) = 1375357710 => 832455
//    01/08/2013 11:49:00 (GMT) = 1375357740 => 182563

#define TEST2_KEY @"GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ" // @"12345678901234567890"
#define TEST2_EXPECTED @[@"176308",@"104105",@"513395",@"188024",@"626094",@"510938",@"151758"]
#define TEST2_STARTTIME 1375357560 // {2013,8,1,12,46,0}
#define TEST2_DIGITS 6

//    secret = GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ
//    01/08/2013 11:46:00 (GMT) = 1375357560 => 176308
//    01/08/2013 11:46:30 (GMT) = 1375357590 => 104105
//    01/08/2013 11:47:00 (GMT) = 1375357620 => 513395
//    01/08/2013 11:47:30 (GMT) = 1375357650 => 188024
//    01/08/2013 11:48:00 (GMT) = 1375357680 => 626094
//    01/08/2013 11:48:30 (GMT) = 1375357710 => 510938
//    01/08/2013 11:49:00 (GMT) = 1375357740 => 151758


@interface OTPTest : XCTestCase
@end

@implementation OTPTest {
    OTP *rawSut;
    OTP *sut;
}

- (void)setUp
{
    [super setUp];
    rawSut = [[OTP alloc] init];
    sut = [[OTP alloc] initWithName:@"testName" andKey:@"testKey"];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testInitWithNameSetsNameProperty
{
    assertThat(sut.name, is(equalTo(@"testName")));
}

- (void)testInitWithNameSetsKeyProperty
{
    assertThat(sut.key, is(equalTo(@"testKey")));
}

- (void)testInitWithNameAndKeyHasDefaultNameWhenNil
{
    sut = [[OTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.name, is(equalTo(@"Unnamed OTP")));
}

- (void)testInitWithNameAndKeyHasDefaultKeyWhenNil
{
    sut = [[OTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.key, is(equalTo(@"")));
}

- (void)testCounterValueStartAt1
{
    rawSut.key = TEST1_KEY;
    assertThat(rawSut.passcode, is(equalTo(@"877291")));
}

- (void)testInitCallsDesignatedInitializer
{
    assertThat(rawSut.name, is(equalTo(@"New OTP")));
}

- (void)testInitialPasscodeWithNilKeyIsNil
{
    assertThat(rawSut.passcode, is(nilValue()));
}

- (void)testOTPGeneratePasscodeReturnsNilForNilKey
{
    assertThat([OTP generatePasscodeForCounter:0 withKey:nil digits:6],is(nilValue()));
}

- (void)testWithTestSet1
{
    [self runTestsForKey:TEST1_KEY expecting:TEST1_EXPECTED startingTime:TEST1_STARTTIME andDigits:TEST1_DIGITS];
}

- (void)testWithTestSet2
{
    [self runTestsForKey:TEST2_KEY expecting:TEST2_EXPECTED startingTime:TEST2_STARTTIME andDigits:TEST2_DIGITS];
}

- (void)runTestsForKey:(NSString *)key expecting:(NSArray *)expectedResults startingTime:(NSInteger)startingTime andDigits:(NSInteger)digits
{
    NSUInteger testCount = [expectedResults count];
    NSUInteger time = startingTime;
    for (NSUInteger t = 0; t < testCount; t += 1) {
        
        assertThat([OTP generatePasscodeForCounter:(time / 30) withKey:key digits:digits],is(equalTo(expectedResults[t])));
        time = time + 30;
    }
}

@end
