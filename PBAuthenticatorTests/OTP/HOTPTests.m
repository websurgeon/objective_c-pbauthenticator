//  HOTPTests.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "HOTP.h"

#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

@interface HOTPTests : XCTestCase
@end

@implementation HOTPTests {
    HOTP *rawSut;
    HOTP *sut;
}

- (void)setUp
{
    [super setUp];
    rawSut = [[HOTP alloc] init];
    sut = [[HOTP alloc] initWithName:@"testHOTP" andKey:@"GAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQ"];
}

- (void)tearDown
{
    rawSut = nil;
    sut = nil;
    [super tearDown];
}

- (void)testInitCallsDesignatedInitializer
{
    // the default name is set by the super class init method
    assertThat(rawSut.name, is(equalTo(@"New HOTP")));
}

- (void)testInitWithNameAndKeyHasDefaultNameWhenNil
{
    sut = [[HOTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.name, is(equalTo(@"Unnamed HOTP")));
}

- (void)testInitWithNameAndKeyHasDefaultKeyWhenNil
{
    sut = [[HOTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.key, is(equalTo(@"")));
}

- (void)testInitialPasscode
{
    assertThat(sut.passcode, is(equalTo(@"877291")));
}

- (void)testPasscodeIncrementedOnce
{
    [sut nextPasscode];
    assertThat(sut.passcode, is(equalTo(@"628880")));
}

- (void)testPasscodeIncrementedTwice
{
    [sut nextPasscode];
    [sut nextPasscode];
    assertThat(sut.passcode, is(equalTo(@"128283")));
}

@end
