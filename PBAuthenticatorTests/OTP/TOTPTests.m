//  TOTPTests.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "TOTP.h"

#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

@interface TOTPTests : XCTestCase
@end

@implementation TOTPTests

- (void)testInitCallsDesignatedInitializer
{
    TOTP *sut = [[TOTP alloc] init];
    // the default name is set by the super class init method
    assertThat(sut.name, is(equalTo(@"New TOTP")));
}

- (void)testInitWithNameAndKeyHasDefaultNameWhenNil
{
    TOTP *sut = [[TOTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.name, is(equalTo(@"Unnamed TOTP")));
}

- (void)testInitWithNameAndKeyHasDefaultKeyWhenNil
{
    TOTP *sut = [[TOTP alloc] initWithName:nil andKey:nil];
    assertThat(sut.key, is(equalTo(@"")));
}

@end
