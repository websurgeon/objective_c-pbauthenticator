//  HOTPTableViewCellTests.m
//  PBAuthenticator
//
//  Created by Peter on 04/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

// Class under test
#import "HOTPTableViewCell.h"

#import <XCTest/XCTest.h>

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>

@interface HOTPTableViewCellTests : XCTestCase
@end

@implementation HOTPTableViewCellTests {
    HOTPTableViewCell *sut;
}

- (void)setUp
{
    [super setUp];
    sut = [[HOTPTableViewCell alloc] init];
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testTargetActionIsSetForNewNextPasscodeButton
{
    UIButton *mockButton = mock([UIButton class]);
    sut.btn_nextPasscode = mockButton;
    
    [verify(mockButton) addTarget:sut action:@selector(nextPasscode:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)testIBActionNextPasscodeInvokesDelegateMethod
{
    id <HOTPTableViewCellDelegate> mockDelegate = mockObjectAndProtocol([NSObject class], @protocol(HOTPTableViewCellDelegate));

    sut.delegate = mockDelegate;
    
    [sut nextPasscode:nil];
    
    [verify(mockDelegate) didTapNextPasscodeButtonForCell:(id)anything()];
}

- (void)testIBActionNextPasscodeDoesNotThrowExceptionWhenNilDelgate
{
    sut.delegate = mock([NSObject class]);
    
    XCTAssertNoThrow([sut nextPasscode:nil]);;
}

@end
