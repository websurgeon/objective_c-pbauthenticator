
# THIS PROJECT IS ON HOLD
Other priorities have come up so no further work is currently being done on this project.  I do still plan to return to it at some point.

# PBAuthenticator

Objective-C implementation of the [Google Authenticator](http://en.wikipedia.org/wiki/Google_Authenticator). 
Google Authenticator is based on [RFC 6238](https://tools.ietf.org/html/rfc6238).  

This app was initially started so as to provide a suitably complex project with which to learn Test Driven Development (TDD) techniques.

After the basic functionality was completed, this project was left untouched for over a year. In which time I have learned so much more about iOS Development and TDD that I now want to bring this project up-to-date.
It is also an oportunity to add some further functionality that is required to make this a 'real' app.

The main goal of this project now is to bring the app to a level of functionality suitable for releasing to the Apple App Store.

Project progress is described in [todo.txt](https://bitbucket.org/websurgeon/objective_c-pbauthenticator/src/develop/todos.txt)



### The MIT License

> Copyright (C) 2013 Peter Barclay.
>
> Permission is hereby granted, free of charge, to any person
> obtaining a copy of this software and associated documentation files
> (the "Software"), to deal in the Software without restriction,
> including without limitation the rights to use, copy, modify, merge,
> publish, distribute, sublicense, and/or sell copies of the Software,
> and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
> BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
> ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
