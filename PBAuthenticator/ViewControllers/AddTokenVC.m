//
//  AddTokenVC.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "AddTokenVC.h"
#import "OTP.h"
#import "HOTP.h"
#import "TOTP.h"
#import <objc/message.h>

#define OTP_TYPE_CHOICE_TOTP 0
#define OTP_TYPE_CHOICE_HOTP 1

@interface AddTokenVC ()

@end

@implementation AddTokenVC

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initSetup];
    }
    return self;
}

- (void)initSetup
{
    _alertViewClass = [UIAlertView class];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"Unwind_addTokenDone"]) {
        BOOL isReady = [self isReadyToCreateOTP];
        if (!isReady) {
            [self showNotReadyAlert];
        }
        return isReady;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Unwind_addTokenDone"]) {
        [self sendDelegateNewOTP:[self createNewOTPFromInputs]];
    }
}

- (void)showNotReadyAlert
{
    UIAlertView *alert = [[_alertViewClass alloc] initWithTitle:@"Missing Value" message:@"Key is required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

- (void)sendDelegateNewOTP:(OTP *)newOTP
{
    if (newOTP && [self.delegate respondsToSelector:@selector(didCreateNewOTP:)]) {
        [self.delegate performSelector:@selector(didCreateNewOTP:) withObject:newOTP];
    }
}

- (OTP *)createNewOTPFromInputs
{
    OTP *newOTP = nil;
    switch ([self.seg_otpChoice selectedSegmentIndex]) {
        case OTP_SUBTYPE_TOTP:
            newOTP = [[TOTP alloc] initWithName:self.input_name.text andKey:self.input_key.text];
            break;
        case OTP_SUBTYPE_HOTP:
            newOTP = [[HOTP alloc] initWithName:self.input_name.text andKey:self.input_key.text];
            break;
    }
    return newOTP;
}

- (BOOL)isReadyToCreateOTP
{
    return [self isValidKey];
}

- (BOOL)isValidKey
{
    return self.input_key.text && ![self.input_key.text isEqualToString:@""];
}

@end
