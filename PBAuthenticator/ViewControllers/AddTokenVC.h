//
//  AddTokenVC.h
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "AddTokenDelegate.h"
#import <UIKit/UIKit.h>

@interface AddTokenVC : UIViewController

typedef enum {
    OTP_SUBTYPE_TOTP = 0,
    OTP_SUBTYPE_HOTP = 1,
} OTP_SUBTYPE;

@property (weak, nonatomic) id <AddTokenDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_cancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_done;
@property (weak, nonatomic) IBOutlet UISegmentedControl *seg_otpChoice;
@property (weak, nonatomic) IBOutlet UITextField *input_name;
@property (weak, nonatomic) IBOutlet UITextField *input_key;

@property (strong, nonatomic) Class alertViewClass;

- (OTP *)createNewOTPFromInputs;

@end
