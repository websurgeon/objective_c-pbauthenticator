//
//  AuthenticatorVC.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "AuthenticatorVC.h"
#import "AddTokenVC.h"
#import "HOTP.h"
#import "TOTP.h"
#import "HOTPTableViewCell.h"
#import "TOTPTableViewCell.h"

@interface AuthenticatorVC ()

@property (nonatomic, strong, readwrite) NSDate *currentDate;

@end

@implementation AuthenticatorVC

@synthesize otpRecords = _otpRecords;

- (NSArray *)otpRecords
{
    if (!_otpRecords) {
//        _otpRecords = @[[[HOTP alloc] initWithName:@"00000000000000000000" andKey:@"GAYDAMBQGAYDAMBQGAYDAMBQGAYDAMBQ"], [[TOTP alloc] initWithName:@"12345678901234567890" andKey:@"GEZDGNBVGY3TQOJQGEZDGNBVGY3TQOJQ"]];
        _otpRecords = @[];
    }
    return _otpRecords;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self startTOTPUpdates];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self stopTOTPUpdates];
}

- (void)startTOTPUpdates
{
    int secondsRemaining = [self countdownSecondsRemaining];
    
    [self.btn_countdown setTitle:[@(secondsRemaining) stringValue]];
    
    if (secondsRemaining == 30) {
        [self.tableView reloadData];
    } else if (secondsRemaining == 5) {
        [self.tableView reloadData];
    }
    [self performSelector:@selector(startTOTPUpdates) withObject:nil afterDelay:1];
}

- (void)stopTOTPUpdates
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startTOTPUpdates) object:nil];
}

- (NSDate *)currentDate
{
    return _currentDate ?: [NSDate date];
}

- (int)countdownSecondsRemaining
{
    return 30 - (int)fmod([[self currentDate] timeIntervalSince1970], 30);
}

- (IBAction)editMode:(id)sender
{
    UIBarButtonItem *btn = self.btn_edit;
    if ([self.tableView isEditing]) {
        [btn setStyle:UIBarButtonItemStyleBordered];
        [btn setTitle:@"Edit"];
    } else {
        [btn setStyle:UIBarButtonItemStyleDone];
        [btn setTitle:@"Done"];
    }
    [self.tableView setEditing:![self.tableView isEditing] animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AuthenticatorVC_btn_to_AddTokenVC"]) {
        AddTokenVC *vc = (AddTokenVC *)segue.destinationViewController;
        vc.delegate = self;
    }
}

#pragma mark - Unwind Segue Methods

- (void)addTokenCancelled:(UIStoryboardSegue *)segue
{
}

- (void)addTokenDone:(UIStoryboardSegue *)segue
{
}

#pragma mark - TableView DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.otpRecords count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdent = @"cell";
    UIColor *textColor = [UIColor blackColor];
    OTP *otp = [self.otpRecords objectAtIndex:indexPath.row];
    if ([otp isKindOfClass:[HOTP class]]) {
        cellIdent = @"cell_hotp";
    } else if ([otp isKindOfClass:[TOTP class]]) {
        cellIdent = @"cell_totp";
        if ([self countdownSecondsRemaining] <= 5) {
            textColor = [UIColor redColor];
        }
    }

    OTPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];

    cell.lbl_name.text = otp.name;
    cell.lbl_passcode.text = otp.passcode;
    [cell.lbl_passcode setTextColor:textColor];

    return cell;
}

#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *tmp = [self.otpRecords mutableCopy];
        [tmp removeObjectAtIndex:indexPath.row];
        self.otpRecords = [NSArray arrayWithArray:tmp];
        [tableView reloadData];
    }
}

#pragma mark - HOTPTableViewCellDelegate Methods

- (void)didTapNextPasscodeButtonForCell:(HOTPTableViewCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    HOTP *hotp = [self.otpRecords objectAtIndex:indexPath.row];
    [hotp nextPasscode];
    [self.tableView reloadData];
}

#pragma mark - AddTokeDelegate Methods

- (void)didCreateNewOTP:(OTP *)otp
{
    self.otpRecords = [self.otpRecords arrayByAddingObject:otp];
    [self.tableView reloadData];
}

@end
