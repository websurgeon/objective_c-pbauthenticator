//
//  AuthenticatorVC.h
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "HOTPTableViewCell.h"
#import "AddTokenDelegate.h"
#import <UIKit/UIKit.h>

@interface AuthenticatorVC : UIViewController <UITableViewDataSource, UITableViewDelegate, HOTPTableViewCellDelegate, AddTokenDelegate>

@property (nonatomic, strong, readonly) NSDate *currentDate;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_addToken;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_edit;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btn_countdown;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *otpRecords;

- (IBAction)addTokenCancelled:(UIStoryboardSegue *)segue;

- (IBAction)addTokenDone:(UIStoryboardSegue *)segue;

- (IBAction)editMode:(id)sender;

- (void)startTOTPUpdates;

- (void)stopTOTPUpdates;

- (int)countdownSecondsRemaining;

#pragma mark - TableView DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - TableView Delegate Methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - HOTPTableViewCellDelegate Methods

- (void)didTapNextPasscodeButtonForCell:(HOTPTableViewCell *)cell;

#pragma mark - AddTokeDelegate Methods

- (void)didCreateNewOTP:(OTP *)otp;

@end
