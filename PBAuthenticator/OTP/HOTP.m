//
//  HOTP.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "HOTP.h"

@interface HOTP()

@property (nonatomic) int counterValue;

@end

@implementation HOTP

- (NSString *)nextPasscode
{
    self.counterValue += 1;
    return self.passcode;
}

@end
