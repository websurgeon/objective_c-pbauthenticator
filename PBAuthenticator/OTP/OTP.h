//
//  OTP.h
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <Foundation/Foundation.h>

@interface OTP : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *key;
@property (readonly, nonatomic) NSString *passcode;

- (id)initWithName:(NSString *)name andKey:(NSString *)key;

+ (NSString *)generatePasscodeForCounter:(NSInteger)counter withKey:(NSString *)rawKey digits:(NSInteger)digits;

@end
