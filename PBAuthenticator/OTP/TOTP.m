//
//  TOTP.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "TOTP.h"

@interface TOTP() {
    
}

@property (nonatomic) int counterValue;

@end

@implementation TOTP

@synthesize counterValue = _counterValue;

- (int)counterValue
{
    int timeValue = (int)[[NSDate date] timeIntervalSince1970];
    _counterValue = timeValue / 30;
    return _counterValue;
}

@end
