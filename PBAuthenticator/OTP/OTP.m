//
//  OTP.m
//  PBAuthenticator
//
//  Created by Peter on 01/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "OTP.h"
#import <CommonCrypto/CommonHMAC.h>
#import "NSData+Base32.h"

@interface OTP()

@property (nonatomic) int counterValue;

@end

@implementation OTP

@synthesize name = _name;

- (NSString *)name
{
    if (!_name) {
        _name = [NSString stringWithFormat:@"Unnamed %@", NSStringFromClass([self class])];
    }
    return _name;
}

@synthesize key = _key;

- (NSString *)key
{
    if (!_key) {
        _key = @"";
    }
    return _key;
}

- (NSString *)passcode
{
    return [self encodeCounterValueToString];
}

- (id)initWithName:(NSString *)name andKey:(NSString *)key
{
    self = [super init];
    if (self) {
        self.name = name;
        self.key = key;
        // 0 is reserved for integrity check so start from 1!
        self.counterValue = 1;
    }
    return self;
}

- (id)init{
    return [self initWithName:[NSString stringWithFormat:@"New %@", NSStringFromClass([self class])]
            andKey:nil];
}

- (NSString *)encodeCounterValueToString
{
    return [OTP generatePasscodeForCounter:self.counterValue withKey:self.key digits:6];
}

// OTP PASSCODE GENERATION
// http://tools.ietf.org/html/rfc4226

// BASE32 ENCODING
// http://tools.ietf.org/html/rfc3548

+ (NSString *)generatePasscodeForCounter:(NSInteger)counter withKey:(NSString *)rawKey digits:(NSInteger)digits
{
    // Google Authenticator is case insensative and copes with unpadded base32
    // so we need to convert to uppercase and then pad the key to make valid length before base32 decoding
    NSString *key = [OTP safeKey:rawKey];
    
    if (!key) return nil;
    
    // EXAMPLE OF HOTP COMPUTATION
    // http://tools.ietf.org/html/rfc4226#section-5.4
    
    // PSUEDOCODE OF Google Authenticator Time & Event/Timer OTP
    // http://en.wikipedia.org/wiki/Google_Authenticator#Pseudocode_for_Time_OTP
    
    // convert counter value to big endian format
    unsigned long long llCounter = counter;
    llCounter = NSSwapHostLongLongToBig(llCounter);
    
    // create key for hmac by converting to bytes
    NSData *keyData = [NSData dataUsingBase32String:key];
    if (!keyData) return nil;
    
    // create message for hmac by converting counter value to bytes
    NSData *message = [NSData dataWithBytes:&llCounter length:sizeof(llCounter)];
    
    // HMAC-SHA1 the message using the key
    unsigned char hmac[20];
	CCHmac(kCCHmacAlgSHA1, [keyData bytes], [keyData length], [message bytes], [message length], hmac);
    
    // get the value in last byte to use as offset
    int offset = hmac[sizeof(hmac)-1] & 0xf;
    
    // We treat the dynamic binary code as a 31-bit, unsigned, big-endian
    // integer; the first byte is masked with a 0x7f
    int binary = (((hmac[offset    ] & 0x7f) << 24) |
                  ((hmac[offset + 1] & 0xff) << 16) |
                  ((hmac[offset + 2] & 0xff) <<  8) |
                  ((hmac[offset + 3] & 0xff)));
    
    // We then take this number modulo 1,000,000 (10^6) to generate the 6-
    // digit HOTP decimal value
    int decimal = binary % (int)pow(10, digits);
    
    // pad the decimal with zeros to make the passcode 6 digits
    NSMutableString *unpaddedPassCode = [NSMutableString stringWithFormat:@"%i", decimal];
    NSMutableString *padding = [@"" mutableCopy];
    for (int i = 0; i < digits - [unpaddedPassCode length]; i += 1) {
        [padding appendString:@"0"];
    }
    NSString *passcode = [NSString stringWithFormat:@"%@%i", padding, decimal];
    
    return passcode;
}

+ (NSString *)safeKey:(NSString *)rawKey
{
    if ([rawKey length] == 0) return nil;
    
    NSString *key = [[rawKey copy] uppercaseString];
    int paddingRequired;
    // ensure that key length is a multiple of 4 by padding with A (to match Google Authenticator results)
    paddingRequired = fmod(4-fmod([key length],4),4);
    if (paddingRequired) {
        key = [NSString stringWithFormat:@"%@%@",key,[@"" stringByPaddingToLength:paddingRequired withString:@"A" startingAtIndex:0]];
    }
    
    // ensure that key length is a multiple of 8 by padding with =
    paddingRequired = fmod(8-fmod([key length],8),8);
    if (paddingRequired) {
        key = [NSString stringWithFormat:@"%@%@",key,[@"" stringByPaddingToLength:paddingRequired withString:@"=" startingAtIndex:0]];
    }
    
    return key;
}

@end
