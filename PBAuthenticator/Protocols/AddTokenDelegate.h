//
//  AddTokenDelegate.h
//  PBAuthenticator
//
//  Created by Peter on 04/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <Foundation/Foundation.h>

@class OTP;

@protocol AddTokenDelegate <NSObject>

@optional

- (void)didCreateNewOTP:(OTP *)otp;

@end
