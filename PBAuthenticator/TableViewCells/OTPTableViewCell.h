//
//  OTPTableViewCell.h
//  PBAuthenticator
//
//  Created by Peter on 02/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <UIKit/UIKit.h>

@interface OTPTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_passcode;

@end
