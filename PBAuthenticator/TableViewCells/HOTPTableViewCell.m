//
//  HOTPTableViewCell.m
//  PBAuthenticator
//
//  Created by Peter on 03/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "HOTPTableViewCell.h"

@implementation HOTPTableViewCell

- (void)setBtn_nextPasscode:(UIButton *)btn_nextPasscode
{
    _btn_nextPasscode = btn_nextPasscode;
    [_btn_nextPasscode addTarget:self action:@selector(nextPasscode:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)nextPasscode:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didTapNextPasscodeButtonForCell:)]) {
        [self.delegate performSelector:@selector(didTapNextPasscodeButtonForCell:) withObject:self];
    }
}

@end
