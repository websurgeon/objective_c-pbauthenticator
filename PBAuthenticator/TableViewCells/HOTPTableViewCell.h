//
//  HOTPTableViewCell.h
//  PBAuthenticator
//
//  Created by Peter on 03/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "OTPTableViewCell.h"

@class HOTPTableViewCell;

@protocol HOTPTableViewCellDelegate <NSObject>

@optional

- (void)didTapNextPasscodeButtonForCell:(HOTPTableViewCell *)cell;

@end

@interface HOTPTableViewCell : OTPTableViewCell

@property (weak, nonatomic) IBOutlet id <HOTPTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btn_nextPasscode;

- (IBAction)nextPasscode:(id)sender;

@end
