//
//  OTPTableViewCell.m
//  PBAuthenticator
//
//  Created by Peter on 02/09/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import "OTPTableViewCell.h"

@implementation OTPTableViewCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeCell];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initializeCell];
    }
    return self;
}

- (void)initializeCell
{
    [[self detailTextLabel] setHidden:YES];
    [[self textLabel] setHidden:YES];
}

// temporary workaround for iOS7 layout bug with delete button being hidden for custom cells designed in XCode 4
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    for (UIView *subview in self.subviews) {
        
        for (UIView *subview2 in subview.subviews) {
            
            if ([NSStringFromClass([subview2 class]) isEqualToString:@"UITableViewCellDeleteConfirmationView"]) { // move delete confirmation view
                
                [subview bringSubviewToFront:subview2];
                
            }
        }
    }
}

@end
