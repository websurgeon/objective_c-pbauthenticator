//
//  NSData+Base32.m
//  PBBase32 (https://bitbucket.org/websurgeon/objective_c-pbbase32)
//
//  Created by Peter on 10/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//
//  MIT Licence
//
//  For more detail on how this encoding algorithm works see https://bitbucket.org/websurgeon/objective_c-pbbase32/src/master/resources/Base32-Process-Notes.md?at=master
//
//  NB: This code is similar to and influenced by ekscrypto's base32 implementation https://github.com/ekscrypto/Base32


#import "NSData+Base32.h"

#define NSSTRING_BASE32_ERROR_DOMAIN @"com.pbbase32.encoding.error"

#define EXCEPTION_No_Input @"NoInputException"
#define EXCEPTION_MESSAGE_No_Input @"No base32 string supplied"

#define EXCEPTION_Invalid_Input_Length @"InputLengthException"
#define EXCEPTION_MESSAGE_Invalid_Input_Length @"Input length was not multiple of 8-bytes"

#define EXCEPTION_Memory_Allocation @"FailedMemoryAllocationException"
#define EXCEPTION_MESSAGE_Memory_Allocation @"Failed to allocate memory for decoding process"

@implementation NSData (Base32)

+ (NSData *)dataUsingBase32String:(NSString *)base32String error:(NSError **)error
{
    NSData *outputData = nil;
    
    unsigned char *decoded = NULL;
    unsigned char *blockBytes = NULL;
    
    @try {
        
        if ([base32String length] == 0) {
            @throw [NSException exceptionWithName:EXCEPTION_No_Input reason:NSLocalizedString(EXCEPTION_MESSAGE_No_Input, @"exception message when no base32 string supplied for decoding") userInfo:nil];
        }
        if ([base32String length] % 8 != 0) {
            @throw [NSException exceptionWithName:EXCEPTION_Invalid_Input_Length reason:NSLocalizedString(EXCEPTION_MESSAGE_Invalid_Input_Length, @"exception message when input string supplied is not valid length for decoding") userInfo:nil];
        }
        
        static NSUInteger paddingAdj[7] = {0, 4, 255, 3, 2, 255, 1}; // 255 = invalid padding count
        
        // a map to convert base32 alphabet char to a position index in "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        // e.g. 'A' = 0, 'B' = 1 ... '7' = 31
        unsigned char base32Lookup[256] = {
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  //   0 -  15 => 0x00 - 0x0f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  //  16 -  31 => 0x10 - 0x1f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  //  32 -  47 => 0x20 - 0x2f
            255,255, 26, 27,  28, 29, 30, 31, 255,255,255,255, 255,255,255,255,  //  48 -  63 => 0x30 - 0x3f
            255,  0,  1,  2,   3,  4,  5,  6,   7,  8,  9, 10,  11, 12, 13, 14,  //  64 -  79 => 0x40 - 0x4f
            15, 16, 17, 18,  19, 20, 21, 22,  23, 24, 25,255, 255,255,255,255,  //  80 -  95 => 0x50 - 0x5f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  //  96 - 111 => 0x60 - 0x6f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 112 - 127 => 0x70 - 0x7f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 128 - 143 => 0x80 - 0x8f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 144 - 159 => 0x90 - 0x9f
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 160 - 175 => 0xa0 - 0xaf
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 176 - 191 => 0xb0 - 0xbf
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 192 - 207 => 0xc0 - 0xcf
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 208 - 223 => 0xd0 - 0xdf
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255,  // 224 - 239 => 0xe0 - 0xef
            255,255,255,255, 255,255,255,255, 255,255,255,255, 255,255,255,255   // 240 - 255 => 0xf0 - 0xff
        };
        
        NSData *inputData = [base32String dataUsingEncoding:NSASCIIStringEncoding];
        
        unsigned char *encoded = (unsigned char *)[inputData bytes];
        NSUInteger encodedLength = [inputData length];
        
        // TODO: ensure that length is multiple of 8
        
        NSUInteger byteIndex = 0;
        NSUInteger padCount = 0;
        NSUInteger decodedByteIndex = 0;
        unsigned char g1, g2, g3, g4, g5, g6, g7, g8;
        
        blockBytes = malloc(8);
        decoded = malloc((encodedLength / 8) * 5);
        
        if (blockBytes == NULL || decoded == NULL) {
            @throw [NSException exceptionWithName:EXCEPTION_Memory_Allocation reason:NSLocalizedString(EXCEPTION_MESSAGE_Memory_Allocation, @"exception message when memory allocation fails") userInfo:nil];
        }
        
        // loop through each byte in input until a full block (8-bytes) is stored
        while (byteIndex < encodedLength) {
            
            // get current byte
            unsigned char inputChar = encoded[byteIndex];
            
            if (inputChar == '=') {
                // skip this byte as it is just padding
                padCount += 1;
                byteIndex++;
                continue;
            }
            
            // map the current byte to a base32 alphabet position index
            blockBytes[byteIndex % 8] = base32Lookup[inputChar];
            
            // ensure that the current byte maps to a valid base32 alphabet char
            if (blockBytes[byteIndex % 8] == 255) {
                // TODO: EXCEPTION - INVALID ENCODING
            }
            
            // once a full block is ready we need to take the last 5-bits of each byte and combine them to
            // make 5 decoded bytes of data
            if ((byteIndex % 8) == 7) {
                g1 = blockBytes[0];
                g2 = blockBytes[1];
                g3 = blockBytes[2];
                g4 = blockBytes[3];
                g5 = blockBytes[4];
                g6 = blockBytes[5];
                g7 = blockBytes[6];
                g8 = blockBytes[7];
                
                decoded[decodedByteIndex++] = ((g1 << 3) & 0xf8) | ((g2 >> 2) & 0x07);
                decoded[decodedByteIndex++] = ((g2 << 6) & 0xc0) | ((g3 << 1) & 0x3e) | ((g4 >> 4) & 0x01);
                decoded[decodedByteIndex++] = ((g4 << 4) & 0xf0) | ((g5 >> 1) & 0x0f);
                decoded[decodedByteIndex++] = ((g5 << 7) & 0x80) | ((g6 << 2) & 0x7c) | ((g7 >> 3) & 0x03);
                decoded[decodedByteIndex++] = ((g7 << 5) & 0xe0) | ((g8     ) & 0x1f);
            }
            
            // increment counter ready for next byte
            byteIndex++;
        }
        
        // convert the number of padding chars to the number of bytes in the partial block
        NSUInteger partialByteCount = paddingAdj[padCount];
        
        if (partialByteCount > 0) {
            // in partial block one or more group will not be available, so initilalise to 0
            g1 = 0;
            g2 = 0;
            g3 = 0;
            g4 = 0;
            g5 = 0;
            g6 = 0;
            g7 = 0;
            g8 = 0;
            
            // construct each byte available in partial block
            switch (partialByteCount) {
                case 255:
                    // TODO: EXCEPTION - INVALID PADDING
                    break;
                case 5:
                    // 5th byte is never created for partial block so no need for this case!
                    //                g8 = blockBytes[7];
                    //                g7 = blockBytes[6];
                    //                decoded[decodedByteIndex + 5] = ((g7 << 5) & 0xe0) | ((g8     ) & 0x1f);
                case 4:
                    // g5, g6 and g7 are required to construct 4th byte
                    g7 = blockBytes[6];
                    g6 = blockBytes[5];
                    g5 = blockBytes[4];
                    decoded[decodedByteIndex + 3] = ((g5 << 7) & 0x80) | ((g6 << 2) & 0x7c) | ((g7 >> 3) & 0x03);
                case 3:
                    // g4 and g5 are required to construct 3rd byte
                    g5 = blockBytes[4];
                    g4 = blockBytes[3];
                    decoded[decodedByteIndex + 2] = ((g4 << 4) & 0xf0) | ((g5 >> 1) & 0x0f);
                case 2:
                    // g2, g3 and g4 are required to construct 2nd byte
                    g4 = blockBytes[3];
                    g3 = blockBytes[2];
                    g2 = blockBytes[1];
                    decoded[decodedByteIndex + 1] = ((g2 << 6) & 0xc0) | ((g3 << 1) & 0x3e) | ((g4 >> 4) & 0x01);
                case 1:
                    // g1 and g2 are required to construct 1st byte
                    g2 = blockBytes[1];
                    g1 = blockBytes[0];
                    decoded[decodedByteIndex] = ((g1 << 3) & 0xf8) | ((g2 >> 2) & 0x07);
            }
            // increment the decoded byte index by the number bytes in the partial block so that we no the decoded length
            decodedByteIndex += partialByteCount;
        }
        
        // convert decoded bytes to NSData
        outputData = [[NSData alloc] initWithBytes:decoded length:decodedByteIndex];
        
    }
    @catch (NSException *exception) {
        if (error != NULL) {
            *error = [NSError errorWithDomain:NSSTRING_BASE32_ERROR_DOMAIN code:1 userInfo:@{@"message":[exception reason], @"exceptoin":exception}];
        }
    }
    @finally {
        // free the allocated memory used for decoding
        if(decoded != NULL) free(decoded);
        if(blockBytes != NULL) free(blockBytes);
    }
    
    
    return outputData;
}

+ (NSData *)dataUsingBase32String:(NSString *)base32String
{
    return [NSData dataUsingBase32String:base32String error:NULL];
}

@end
