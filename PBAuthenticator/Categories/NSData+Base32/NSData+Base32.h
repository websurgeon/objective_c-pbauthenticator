//
//  NSData+Base32.h
//  PBBase32
//
//  Created by Peter on 10/08/2013.
//  Copyright (c) 2013 Peter Barclay. See Licence.txt
//

#import <Foundation/Foundation.h>

@interface NSData (Base32)

+ (NSData *)dataUsingBase32String:(NSString *)base32String error:(NSError **)error;

+ (NSData *)dataUsingBase32String:(NSString *)base32String;

@end
